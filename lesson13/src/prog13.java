import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
public class prog13 {


	public static void main(String[] args) {
		
		Window window = new Window();

	}

}

class Window extends JFrame
{
	public Window()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds (0, 0, 800, 600);
		setTitle ("��������� ����������� ��������� �����");
		setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		ColorPanel panel1 = new ColorPanel();
		final DrawingPanel panel2 = new DrawingPanel();
		panel1.setColorChangeListener(new ColorChangeListener() {
			
			@Override
			public void onColorChanged(Color color) {
				panel2.setColor(color);
			}
		});
		Container con = getContentPane();
		con.add(BorderLayout.WEST, panel1);
		con.add(BorderLayout.CENTER, panel2);
		setVisible(true);
		setFocusable (true);
	}
}

class ColorPanel extends JPanel
{
	Color[] masColor;
	int tCol = 0;
	ColorChangeListener mColorListener;
	
	public ColorPanel()	{
		addMouseListener(new myMouse1());
		addMouseMotionListener(new myMouse2());
		setPreferredSize(new Dimension(100, 525));
		setMaximumSize(new Dimension(100, 525));
	}
	
	public void setColorChangeListener(ColorChangeListener colorChangeListener) {
		mColorListener = colorChangeListener;
	}

	@Override
	public void paintComponent(Graphics gr)
	{
		masColor = new Color[7];
		masColor[0] = (new Color(10, 1, 255));
		masColor[1] = Color.magenta;
		masColor[2] = (new Color(110, 80, 30));
		masColor[3] = Color.LIGHT_GRAY;
		masColor[4] = (new Color(17, 180, 12));
		masColor[5] = Color.BLACK;
		masColor[6] = Color.white;
		for (int i=0; i<7; i++)
		{
			gr.setColor(masColor[i]);
			gr.fillRect(0, i*75, 100, 75);
		}
		gr.drawRect(0, 0, getBounds().width, getBounds().height);
	}
	
	public class myMouse1 implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent arg0) 
		{
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) 
		{
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) 
		{
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) 
		{
			int tX = arg0.getX();
			int tY = arg0.getY();
			int col = arg0.getClickCount();
			int btn = arg0.getButton();
			if ((tX > 0) && (tX < 100) && (tY > 0) && (tY < 700))
			{
				if (col==1)
				{
				 if (btn==1)
			     {
					tCol = tY / 75;
					if (mColorListener != null) {
						mColorListener.onColorChanged(masColor[tCol]);
					}
				 }
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent arg0) 
		{
			
		}
		
	}
	
	public class myMouse2 implements MouseMotionListener
	{

		@Override
		public void mouseDragged(MouseEvent arg0) 
		{
		}

		@Override
		public void mouseMoved(MouseEvent arg0) 
		{
			int tX = arg0.getX();
			int tY = arg0.getY();
			if ((tX > 0) && (tX < 100) && (tY > 0) && (tY < 525))
			{
				setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
			else
			{
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		}
	}
}

interface ColorChangeListener {
	void onColorChanged(Color color);
}

class DrawingPanel extends JPanel {
	private class ColoredPoint {
		Color color;
		int x,y;
		public ColoredPoint(int x, int y, Color color) {
			this.color = color;
			this.x = x;
			this.y = y;
		}
	}

	private Color mCurrentColor;
	ArrayList<ColoredPoint> mPoints = new ArrayList<ColoredPoint>(1000);
	
	public DrawingPanel() {
		addMouseMotionListener(new Drawer());
	}
	
	@Override
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		for (ColoredPoint point : mPoints) {
			graphics.setColor(point.color);
			graphics.fillOval(point.x, point.y, 8, 8);			
		}
	}
	
	public void setColor(Color color) {
		mCurrentColor = color;
	}
	
	class Drawer implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			mPoints.add(new ColoredPoint(e.getX(), e.getY(), mCurrentColor));
			repaint();
		}

		@Override
		public void mouseMoved(MouseEvent e) {
		}
		
	}
}
