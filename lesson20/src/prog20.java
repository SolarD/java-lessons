import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class prog20 extends JFrame {

	JTextArea text = new JTextArea();
	private double v1, v2;
	private int operation = 0;
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					prog20 frame = new prog20();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public prog20() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setForeground(Color.GREEN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel l1 = new JLabel("Результат");
		l1.setFont(new Font("Arial", Font.BOLD, 40));
		l1.setForeground(new Color(4, 255, 6));
		l1.setBounds(10, 10, 214, 52);
		contentPane.add(l1);
		
		text.setEditable(false);
		text.setFont(new Font("Arial", Font.PLAIN, 40));
		text.setBounds(10, 73, 440, 52);
		text.setForeground(new Color(4, 255, 6));
		contentPane.add(text);
		
		JButton button = new JButton("7");
		button.setFont(new Font("Arial", Font.BOLD, 40));
		button.setForeground(Color.GREEN);
		button.setBounds(10, 136, 80, 80);
		contentPane.add(button);
		
		JButton btnNewButton = new JButton("8");
		btnNewButton.setForeground(Color.GREEN);
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 40));
		btnNewButton.setBounds(100, 136, 80, 80);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("9");
		btnNewButton_1.setFont(new Font("Arial", Font.BOLD, 40));
		btnNewButton_1.setForeground(Color.GREEN);
		btnNewButton_1.setBounds(190, 136, 80, 80);
		contentPane.add(btnNewButton_1);
		
		JButton button_1 = new JButton("4");
		button_1.setFont(new Font("Arial", Font.BOLD, 40));
		button_1.setForeground(Color.GREEN);
		button_1.setBounds(10, 225, 80, 80);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("5");
		button_2.setForeground(Color.GREEN);
		button_2.setFont(new Font("Arial", Font.BOLD, 40));
		button_2.setBounds(100, 225, 80, 80);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("6");
		button_3.setFont(new Font("Arial", Font.BOLD, 40));
		button_3.setForeground(Color.GREEN);
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button_3.setBounds(190, 225, 80, 80);
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("1");
		button_4.setForeground(Color.GREEN);
		button_4.setFont(new Font("Arial", Font.BOLD, 40));
		button_4.setBounds(10, 316, 80, 80);
		contentPane.add(button_4);
		
		JButton btnNewButton_2 = new JButton("2");
		btnNewButton_2.setFont(new Font("Arial", Font.BOLD, 40));
		btnNewButton_2.setForeground(Color.GREEN);
		btnNewButton_2.setBounds(100, 316, 80, 80);
		contentPane.add(btnNewButton_2);
		
		JButton button_5 = new JButton("3");
		button_5.setFont(new Font("Arial", Font.BOLD, 40));
		button_5.setForeground(Color.GREEN);
		button_5.setBounds(190, 316, 80, 80);
		contentPane.add(button_5);
		
		JButton button_6 = new JButton("+");
		button_6.setForeground(Color.GREEN);
		button_6.setFont(new Font("Arial", Font.BOLD, 40));
		button_6.setBounds(280, 136, 80, 80);
		contentPane.add(button_6);
		
		JButton button_7 = new JButton("*");
		button_7.setForeground(Color.GREEN);
		button_7.setFont(new Font("Arial", Font.BOLD, 40));
		button_7.setBounds(280, 225, 80, 80);
		contentPane.add(button_7);
		
		JButton button_8 = new JButton("-");
		button_8.setForeground(Color.GREEN);
		button_8.setFont(new Font("Arial", Font.BOLD, 40));
		button_8.setBounds(370, 136, 80, 80);
		contentPane.add(button_8);
		
		JButton btnNewButton_3 = new JButton("/");
		btnNewButton_3.setFont(new Font("Arial", Font.BOLD, 40));
		btnNewButton_3.setForeground(new Color(0, 255, 0));
		btnNewButton_3.setBounds(370, 225, 80, 80);
		contentPane.add(btnNewButton_3);
		
		JButton button_9 = new JButton("=");
		button_9.setForeground(Color.GREEN);
		button_9.setFont(new Font("Arial", Font.BOLD, 40));
		button_9.setBounds(10, 405, 170, 80);
		contentPane.add(button_9);
		
		JButton btnKk = new JButton("kv");
		btnKk.setFont(new Font("Arial", Font.BOLD, 40));
		btnKk.setForeground(Color.GREEN);
		btnKk.setBounds(280, 316, 80, 80);
		contentPane.add(btnKk);
		
		JButton btnKy = new JButton("ky");
		btnKy.setFont(new Font("Arial", Font.BOLD, 40));
		btnKy.setForeground(Color.GREEN);
		btnKy.setBounds(370, 316, 80, 80);
		contentPane.add(btnKy);
		
		JButton btnKk_1 = new JButton("kk");
		btnKk_1.setForeground(Color.GREEN);
		btnKk_1.setFont(new Font("Arial", Font.BOLD, 40));
		btnKk_1.setBounds(370, 405, 80, 80);
		contentPane.add(btnKk_1);
		
		JButton btnC = new JButton("C");
		btnC.setForeground(Color.GREEN);
		btnC.setFont(new Font("Arial", Font.BOLD, 40));
		btnC.setBounds(190, 405, 80, 80);
		contentPane.add(btnC);
		
		JButton button_10 = new JButton(".");
		button_10.setForeground(Color.GREEN);
		button_10.setFont(new Font("Arial", Font.BOLD, 40));
		button_10.setBounds(280, 405, 80, 80);
		contentPane.add(button_10);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setForeground(Color.GREEN);
		btnExit.setFont(new Font("Arial", Font.BOLD, 40));
		btnExit.setBounds(234, 11, 216, 52);
		contentPane.add(btnExit);
		
		
		
		
	}
}
