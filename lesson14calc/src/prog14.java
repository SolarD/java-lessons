import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class prog14 {


	public static void main(String[] args) {
		
		Window window = new Window();
		window.setVisible(true)
;	}

}

class Window extends JFrame
{
	private TextArea text;
	private int operation = 0; // 1+, 2-, 3*, 4/
	private double v, v1, v2;
	private double res = 0;
	private JButton[] btn;
	private String str;
	
	
	public class Key implements KeyListener
	{

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			System.out.println(key);
			for (int i = 0; i < 20; i++)
			{
				if (e.getKeyChar() == btn[i].getText().charAt(0))
				{
					btn[i].doClick();
				}
			}
			if (key == 96)
			{
				str = "0";
			}
			if (key == 97)
			{
				str = "1";
			}
			if (key == 98)
			{
				str = "2";
			}
			if (key == 99)
			{
				str = "3";
			}
			if (key == 100)
			{
				str = "4";
			}
			if (key == 101)
			{
				str = "5";
			}
			if (key == 102)
			{
				str = "6";
			}
			if (key == 103)
			{
				str = "7";
			}
			if (key == 104)
			{
				str = "8";
			}
			if (key == 105)
			{
				str = "9";
			}
			if (key == 106)
			{
				str = "*";
			}
			if (key == 107)
			{
				str = "+";
			}
			if (key == 109)
			{
				str = "-";
			}
			if (key == 111)
			{
				str = "/";
			}
			if (key == 27)
			{
				str = "exit";
			}
			if (key == 46)
			{
				str = "C";
			}
			if (key == 13)
			{
				str = "=";
			}
			if (key == 190)
			{
				str = ".";
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			
		}

		@Override
		public void keyTyped(KeyEvent e) {
			
		}
		
	}
	
	public Window()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds (0, 0, 800, 600);
		setTitle ("Calculator");
		
		Font fontButton = new Font("Areal",1,18);
		Font fontLable = new Font ("TimeNewRomain",1,30);
		JPanel panel = new JPanel();
		Container cont = getContentPane();
		panel.setLayout(null);
		panel.setFocusable(true);
		panel.addKeyListener(new Key());
		btn = new JButton[21];
		for (int i=0; i<21; i++)
		{
			btn[i] = new JButton();
			btn[i].setText(""+i);
			btn[i].setForeground(new Color(2, 244, 5));
			btn[i].setFont(fontButton);
			btn[i].addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String str = ((JButton) e.getSource()).getText();
					if (str == "exit")
					{
						System.exit(0);
					}
					else if (str.equals("C"))
					 {
						text.setText("");
						v1 = v2 = 0;
					 }
					else if (str.equals("="))
					{
						if (text.getText().equals("")){
							text.setText("0");
						}
						
						v2 = Double.parseDouble(text.getText());
						if (operation == 1) text.setText(""+( v1 + v2));
					    if (operation == 2) text.setText(""+( v1 - v2));
				        if (operation == 3) text.setText(""+( v1 * v2));
			            if (operation == 4) text.setText(""+( v1 / v2));
			            if (operation == 5) text.setText(""+( v1 * v1));
			            if (operation == 6) text.setText(""+( v1 * v1 * v1));
			            if (operation == 7) text.setText(""+(Math.sqrt(v1)));
					}
					else if (str.equals("+"))
					{
						operation = 1;
						v1 = (Double.parseDouble(text.getText()));
						text.setText("");
					}
					else if (str.equals("-"))
					{
						operation = 2;
						v1 = (Double.parseDouble(text.getText()));
						text.setText("");
					}
					else if (str.equals("*"))
					{
						operation = 3;
						v1 = (Double.parseDouble(text.getText()));
						text.setText("");
					}
					else if (str.equals("/"))
					{
						operation = 4;
						v1 = (Double.parseDouble(text.getText()));
						text.setText("");
					}
					else if (str.equals("kv"))
					{
						operation = 5;
						v1 = (Double.parseDouble(text.getText()));
						text.setText(v1 * v1 + "" );
					}
					else if (str.equals("ky"))
					{
						operation = 6;
						v1 = (Double.parseDouble(text.getText()));
						text.setText(v1 * v1 * v1 + "" );
					}
					else if (str.equals("kk"))
					{
						operation = 7;
						v1 = (Double.parseDouble(text.getText()));
						text.setText(v1 + "");
					}
					else if (str.equals("=") && (operation == 4) && (v2 == 0))
					{
						JOptionPane.showMessageDialog(null, "");
					}	
					else {
						text.setText(text.getText() + str);
					}
					
						
				}
			});
			
			panel.add(btn[i]);
			
			
		}
		
		btn[7].setBounds(10, 122, 80, 80);
		btn[8].setBounds(91, 122, 80, 80);
		btn[9].setBounds(172, 122, 80, 80);
		btn[4].setBounds(10, 203, 80, 80);
		btn[5].setBounds(91, 203, 80, 80);
		btn[6].setBounds(172, 203, 80, 80);
		btn[1].setBounds(10, 284, 80, 80);
		btn[2].setBounds(91, 284, 80, 80);
		btn[3].setBounds(172, 284, 80, 80);
		btn[0].setBounds(10, 365, 161, 80);
		btn[10].setBounds(253, 122, 80, 80);
		btn[11].setBounds(253, 203, 80, 80);
		btn[12].setBounds(253, 284, 80, 80);
		btn[13].setBounds(253, 365, 80, 80);
		btn[14].setBounds(172, 365, 80, 80);
		btn[15].setBounds(172, 446, 161, 80);
		btn[16].setBounds(10, 446, 161, 80);
		btn[17].setBounds(334, 122, 80, 161);
		btn[18].setBounds(415, 122, 80, 161);
		btn[19].setBounds(334, 284, 80, 161);
		btn[20].setBounds(450, 450, 90, 90);
		
		
		btn[10].setText("+");
		btn[11].setText("-");
		btn[12].setText("*");
		btn[13].setText("/");
		btn[14].setText("C");
		btn[15].setText("=");
		btn[16].setText("exit");
		btn[17].setText("kv");
		btn[18].setText("ky");
		btn[19].setText("kk");
		btn[20].setText(".");
		
		JLabel label = new JLabel();
		label.setBounds(10, 10, 300, 50);
		label.setText("Result");
		label.setForeground(new Color(2, 244, 5));
		label.setFont(fontLable);
		panel.add(label);
		label.setFocusable(true);
		
		text = new TextArea();
		text.setBounds(10, 61, 323, 50);
		text.setFont(fontLable);
		text.setBackground(this.getBackground());
		text.setForeground(new Color(2, 224, 5));
		text.setEditable(false);
		panel.add(text);
		
		cont.add(panel);
	}
	
	}
