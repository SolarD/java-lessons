import java.util.List;
import java.util.ArrayList;


public class lesson4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Shool s1 = new Shool();
		s1.name ="����� �2";
		s1.col_floors = 3;
		s1.col_classroms = 39;
		
		Class c1 = new Class("8�");
		c1.form_master = new ClassMaster("������", "�", "�");
		c1.classroom = "k27";

		s1.classes.add(c1);
		
		c1.pupils.add(new Pupil("�������", "����", "�������������", 2.7f));
		c1.pupils.add(new Pupil("asdasd", "asfsdf", "sdfsdf", 9f));
		
		System.out.println(s1.getPupilsCount());
	}

}

class Shool {
	String name;
	int col_floors;
	int col_classroms;
	
	List<Class> classes = new ArrayList<Class>();
	
	int getPupilsCount() {
		int count = 0;
		for (Class c : classes) {
			count += c.pupils.size();
		}
		return count;
	}
}
class Class {
	private String name;
	ClassMaster form_master;
	String classroom;
	ArrayList<Pupil> pupils = new ArrayList<Pupil>();
	
	public Class(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}

class Person {
	String firstName;
	String lastName;
	String middleName;
	
	public Person(String lastName, String firstName, String middleName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
	}
}

class Teacher extends Person {

	public Teacher(String lastName, String firstName, String middleName) {
		super(lastName, firstName, middleName);
	}
	
}

class ClassMaster extends Teacher {
	Class clazz;
	
	public ClassMaster(String lastName, String firstName, String middleName) {
		super(lastName, firstName, middleName);
	}
}

class Pupil extends Person {

	float sredniy_bal;
	
	public Pupil(String firstName, String lastName, String middleName, float avg) {
		super(firstName, lastName, middleName);
		sredniy_bal = avg;
	}
}